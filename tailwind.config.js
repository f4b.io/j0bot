const Colorkraken = require("colorkraken");
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    "./public/**/*.html",
    "./src/**/*.vue",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["Open Sans", ...defaultTheme.fontFamily.sans]
      },
      colors: Colorkraken,
      textColors: Colorkraken,
      backgroundColors: Colorkraken
    }
  },
  variants: {
    extend: {
      opacity: ["disabled"]
    }
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography")
  ]
};
