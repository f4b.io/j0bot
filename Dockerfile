FROM node:latest

RUN apt-get --yes update
RUN apt-get --yes install apt-utils git optipng build-essential
RUN npm install --global npm node-gyp
WORKDIR /app
COPY . .

RUN npm ci
RUN npm run build

FROM nginx:mainline-alpine

COPY --from=0 /app/dist /usr/share/nginx/html

RUN /bin/chown -R nginx:nginx /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
