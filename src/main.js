import Vue from "vue";
import VueLogger from "vuejs-logger";
import App from "./App.vue";
import "./index.scss";

const isProduction = process.env.NODE_ENV === "production";
const loggerOptions = {
  isEnabled: true,
  logLevel: isProduction ? "error" : "debug",
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: "|",
  showConsoleColors: true
};

Vue.config.productionTip = isProduction;
Vue.use(VueLogger, loggerOptions);

new Vue({
  render: h => h(App),
}).$mount("#app");
