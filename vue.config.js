// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = {
  configureWebpack: {
    optimization: {
      minimize: false
    },
    // module: {
    //   rules: [
    //     {
    //       test: /\.s[ac]ss$/i,
    //       use: [
    //         "style-loader",
    //         {
    //           loader: MiniCssExtractPlugin.loader,
    //           options: {
    //             esModule: false,
    //           },
    //         },
    //         { loader: "css-loader", options: { sourceMap: true } },
    //         { loader: "sass-loader", options: { sourceMap: true } },
    //         "postcss-loader"
    //       ]
    //     },
    //   ]
    // },
    plugins: [
      new ESLintPlugin(),
      new FaviconsWebpackPlugin("./public/favicon.png"),
    ],
  }
};
